"""
Create a docker image localy with the rightful tag and deploy it onto Gitlab.

"""

import argparse
import os
import subprocess



class build_docker_image(object):
    """
    Build the Docker image and put it in the Gitlab registry
    """
    
    def __init__(self):
        
        parser = argparse.ArgumentParser(
            description='Build the Docker image and put it in the Gitlab registry.',
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        args = self._interpret_command_line(parser)
        
        # We would like to push directly into MoReFEM registry, but it is not yet possible (a ticket is open for that:
        # https://gitlab.com/gitlab-org/gitlab/issues/22743).
        # Once it's available, use the commented line (and change Yaml accordingly):
        # registry = registry.gitlab.inria.fr/morefem/corelibrary/morefem
        registry = "registry.gitlab.inria.fr/morefem/corelibrary/generatedockerimages"
        
        tag_latest = ""
        
        if args.do_update_latest:
            tag_latest = "-t {registry}/{os}_{compiler}_{mode}_shared:latest".format(mode = args.mode, compiler = args.compiler, os = args.os, registry = registry)
                
        cmd = "docker build --squash -t  {registry}/{os}_{compiler}_{mode}_shared:{tag} {tag_latest} --build-arg os={os} --build-arg mode={mode} --build-arg compiler={compiler} --build-arg is_single_library={is_single_library} --build-arg  library_type=shared .".format(mode = args.mode, compiler = args.compiler, os = args.os, tag = args.tag, tag_latest=tag_latest, is_single_library = args.is_single_library, registry = registry)
                
        #print(cmd)
        subprocess.run(cmd, shell=True, check=True)
        

    def _interpret_command_line(self, parser):
        r"""Interpret the content of the command line.
    
        \param[in] The argparse object.
    
        \return The arguments of the command line.
        """
        
        def check_mode(value):
            if value not in('debug', 'release'):
                raise argparse.ArgumentTypeError("Mode should be either 'debug' or 'release'; you chose \'{}\'.".format(value))
            return value
        
        parser.add_argument(
            '--mode',
            type=check_mode,
            required=True,
            help='Either "debug" or "release".'
        )
        
        def check_os(value):
            if value not in('ubuntu', 'fedora'):
                raise argparse.ArgumentTypeError("OS should be either 'ubuntu' or 'fedora'; you chose \'{}\'.".format(value))
            return value
        
        parser.add_argument(
            '--os',
            type=check_os,
            required=True,            
            help='Either "ubuntu" or "fedora".'
        )
        
        def check_compiler(value):
            if value not in('gcc', 'clang'):
                raise argparse.ArgumentTypeError("Compiler should be either 'gcc' or 'clang'; you chose \'{}\'.".format(value))
            return value
        
        parser.add_argument(
            '--compiler',
            type=check_compiler,
            required=True,            
            help='Either "gcc" or "clang".'
        )
        
        
        parser.add_argument(
            '--tag',
            required=True,            
            help='Tag to use for the Docker images'
        )
        
        
        # Lifted from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
        def str2bool(v):
            if isinstance(v, bool):
               return v
            if v.lower() in ('yes', 'true', 't', 'y', '1'):
                return True
            elif v.lower() in ('no', 'false', 'f', 'n', '0'):
                return False
            else:
                raise argparse.ArgumentTypeError('Boolean value expected.')
        
        parser.add_argument(
            '--is_single_library',
            type=str2bool,
            required=True,
            help='If true, all MoReFEM will be packed in a single library.'
        )
        
        
        parser.add_argument(
            '--do_update_latest',
            type=str2bool,
            required=True,
            help='If true, "latest" tag will be given to the generated Docker image'
        )
    
        args = parser.parse_args()
    
        return args


if __name__ == "__main__":
    
    build_docker_image()





