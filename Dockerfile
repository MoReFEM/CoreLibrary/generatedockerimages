# ================================================================================================================
# This Dockerfile assumes a local image onto which third party libraries are built properly is available.
# See https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory to see how it was generated.
# ================================================================================================================

ARG os
ARG mode
ARG compiler
ARG is_single_library
ARG library_type

FROM registry.gitlab.inria.fr/morefem/thirdpartycompilationfactory/${os}-${compiler}-${mode} AS third_party_container
LABEL maintainer Sébastien Gilles "sebastien.gilles@inria.fr"

# Repeat is mandatory here - see https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG os
ARG mode
ARG compiler
ARG is_single_library
ARG library_type

USER root
WORKDIR /home/${USER}/Codes/MoReFEM
RUN chmod 777 /opt
RUN chown -R ${USER} .

USER ${USER}

RUN mkdir -p build_docker Documentation ExternalTools/ClangFormat

COPY Sources Sources
COPY cmake cmake
COPY Scripts Scripts
COPY CMakeLists.txt CMakeLists.txt
COPY Data Data
COPY Documentation/CopyrightNotice.txt Documentation/CopyrightNotice.txt 
COPY ExternalTools/ClangFormat/clang-format ExternalTools/ClangFormat/clang-format 

WORKDIR /home/${USER}/Codes/MoReFEM/build_docker

RUN python3 ../cmake/Scripts/configure_cmake.py --cache_file=../cmake/PreCache/linux.cmake --cmake_args="-G Ninja"  --install_directory=/opt --third_party_directory=/opt --morefem_as_single_library=${is_single_library} --mode=${mode} --library_type=${library_type}

RUN ninja    
RUN ctest
RUN ninja install

WORKDIR /home/non_root_user/Codes
USER root
RUN rm -rf MoReFEM /tmp/*
USER ${USER}

FROM registry.gitlab.inria.fr/morefem/thirdpartycompilationfactory/${os}-${compiler}-${mode}

COPY --from=third_party_container /opt/MoReFEM /opt/MoReFEM 