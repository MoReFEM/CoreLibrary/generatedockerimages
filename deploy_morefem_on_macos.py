"""
Deploy on macOS VM the newest version of MoReFEM or not - depending on command line argument.


"""

import argparse
import os
import subprocess



class deploy_on_macos():
    """
    Deploy on macOS VM the newest version of MoReFEM or not - depending on command line argument.
    """
    
    def __init__(self):
        
        parser = argparse.ArgumentParser(
            description='Deploy on macOS VM the newest version of MoReFEM or not - depending on command line argument.',
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        args = self._interpret_command_line(parser)
                
        if args.update_installed_morefem:
            subprocess.run("ninja install", shell=True, check=True)
        

    def _interpret_command_line(self, parser):
        """Interpret the content of the command line.
    
        \param[in] The argparse object.
    
        \return The arguments of the command line.
        """
        
        # Lifted from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
        def str2bool(v):
            if isinstance(v, bool):
               return v
            if v.lower() in ('yes', 'true', 't', 'y', '1'):
                return True
            elif v.lower() in ('no', 'false', 'f', 'n', '0'):
                return False
            else:
                raise argparse.ArgumentTypeError('Boolean value expected.')
    
        parser.add_argument(
            '--update_installed_morefem',
            type=str2bool,
            required=True,
            help='If true, overwrite the installation of MoReFEM on the VM.'
        )
    
        args = parser.parse_args()
    
        return args


if __name__ == "__main__":
    
    deploy_on_macos()





