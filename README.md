The purpose of this project is to generate 3 Docker images with a functioning MoReFEM embedded. The three cases are:

- Ubuntu os with gcc compiler; MoReFEM is generated as several shared libraries in debug mode.
- Fedora os with clang compiler; MoReFEM is generated as a single shared library in debug mode.
- Fedora os with clang compiler; MoReFEM is generated as a several shared libraries in release mode.

It assumes the images for the third party dependencies were created first (see [the dedicated project](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory)).


# How to create an image?

Go to the CI pipeline page, and set the following variables:

- Add a variable named __MOREFEM_BRANCH__  and associate the name of the branch from which Docker image is generated. This must be a valid branch of the [main MoReFEM project.](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM).
- Add a variable named __TAG__ and associate the name of the tag to use (e.g. _v20.10_). This is a tag for MoReFEM; so far it is assumed the _latest_ tag is used as the base image for third party libraries.
- Add a variable named __UPDATE_LATEST_TAG__ and associate _True_ or _False_, depending on whether you want to overwrite the _latest_ tag of the Docker images.

Then choose _Run pipeline_; on success the images are generated and put in this [project registry](https://gitlab.inria.fr/MoReFEM/CoreLibrary/generatedockerimages/container_registry) (the goal would be to put it directly in )[MoReFEM registry](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/container_registry) but it is not yet possible, as mentioned in [this ticket on Gitlab project](https://gitlab.com/gitlab-org/gitlab/issues/22743).



# Using an image

The best is to provide an example:


````
docker pull registry.gitlab.inria.fr/morefem/corelibrary/morefem/fedora_clang_debug_shared
docker run -it registry.gitlab.inria.fr/morefem/corelibrary/morefem/fedora_clang_debug_shared
````

And inside the container clone a model and run its tests:

````
git clone https://gitlab.inria.fr/MoReFEM/Models/AcousticWave
cd AcousticWave
mkdir build
cd build
python /opt/MoReFEM/cmake/configure_cmake_external_model.py --morefem_install_dir=/opt/MoReFEM --cmake_args="-G Ninja" 
ninja
ctest
````





